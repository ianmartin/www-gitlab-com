---
layout: markdown_page
title: "Underperformance"
---

At GitLab, we strive to hire smart people who can get things done, and the bar
to entry is already high with our interview process. However, the interview
process is imperfect, and people may not live up to expectations. We want
people to be successful and give every opportunity to each individual to work
effectively. At the same time, we need to balance the company’s needs and move
quickly to handle underperformance.

In a scenario when it's clear to a manager that someone isn't accomplishing
enough or working well with others, here is a guideline for how to handle
underperformance. Note that this is only a guideline, and people may be let
go without undertaking these steps--something we wish to avoid if possible.

1) Manager communicates to direct report during one-on-one that team member
needs to improve. If there are extenuating circumstances, some leeway is
granted, depending on the situation.

2) Otherwise, manager documents a [performance improvement plan (PIP)](https://www.shrm.org/templatestools/howtoguides/pages/performanceimprovementplan.aspx) and
shares with direct report. This plan includes:

   * Evaluation of current work by manager(s)
   * Clear metrics and concrete goals to improve (e.g. finish X before Y)
   * Resources/coaching necessary to achieve goals

The manager should consider involving People Ops to help ensure consistency in the PIP process for any affected team member, and also to help protect GitLab should legal claims arise at some point in the future. People Ops does not need to be involved in the creation of the PIP (although People Ops can help), but People Ops should be notified of what is happening.


3) Team member gets time (e.g. 2-4 weeks; this can be longer or shorter and depends on the role and the circumstances) to demonstrate improvements and meet the goals that are outlined in the performance improvement plan. If insufficient improvements are made, the period for the performance improvement plan may be extended, at the discretion of the manager.

4) Otherwise, the team member is let go or his/her contract is [cancelled](/handbook/people-operations/#involuntary-terminations).

5) Manager writes a debrief:

   1. How could this have been avoided?
   2. Were there early signs that were missed?
   3. In retrospect, what questions should have been asked? For example, "How
      would you compare yourself relative to your peers?" People are surprisingly
      honest here.

By using this process, letting people go should not be a surprise to the person in question, but it should be a surprise to the company. If a person does need to be let go, follow the process for [involuntary termination](/handbook/people-operations/#involuntary-terminations) and the [offboarding steps](/handbook/offboarding/).
